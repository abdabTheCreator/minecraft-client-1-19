FROM ubuntu:jammy

RUN mkdir minecraftforge
WORKDIR minecraftforge

ENV DISPLAY=${DISPLAY}
VOLUME [ "/tmp/.X11-unix:/tmp/.X11-unix" ]

### Install needed dependencies
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get install -y curl wget unzip openjdk-17-jdk

RUN apt-get update && apt-get install -y firefox x11vnc xvfb
RUN echo "exec firefox" > ~/.xinitrc && chmod +x ~/.xinitrc

### get minecraft from forge, unzip and remove folder
RUN wget "https://maven.minecraftforge.net/net/minecraftforge/forge/1.19.4-45.0.43/forge-1.19.4-45.0.43-mdk.zip" -O temp.zip
RUN unzip temp.zip && rm temp.zip

RUN ./gradlew genEclipseRun && ./gradlew build

CMD ["v11vnc", "-create", "-forever"]
